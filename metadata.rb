# frozen_string_literal: true

name 'jmx-exporter'
maintainer 'Make.org'
maintainer_email 'sre@make.org'
license 'Apache-2.0'
description 'Cookbook used to install and configure jmx_exporter'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/jmx-exporter'
issues_url 'https://gitlab.com/chef-platform/jmx-exporter/issues'
version '1.0.0'

supports 'centos', '>= 7.1'

chef_version '>= 12.19'
