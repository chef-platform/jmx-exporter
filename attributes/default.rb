# frozen_string_literal: true

# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
cookbook_name = 'jmx-exporter'

default[cookbook_name]['prefix_home'] = '/opt' # where is link to install dir

# Directory where jmx exporter is installed
default[cookbook_name]['path'] =
  "#{node[cookbook_name]['prefix_home']}/jmx_exporter"

# Name of the binary
default[cookbook_name]['binary'] =
  "#{node[cookbook_name]['path']}/jmx_exporter.jar"

# User and group
default[cookbook_name]['user'] = 'jmx_exporter'
default[cookbook_name]['group'] = 'jmx_exporter'

# Version of jmx_exporter
default[cookbook_name]['version'] = '0.11.0'
version = node[cookbook_name]['version']

# Maven repository where jar is hosted
maven_base_url =
  'https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver'
jar = "jmx_prometheus_httpserver-#{version}-jar-with-dependencies.jar"

default[cookbook_name]['repo'] = "#{maven_base_url}/#{version}/#{jar}"

# Java package to install by platform
default[cookbook_name]['java'] = {
  'centos' => 'java-1.8.0-openjdk-headless'
}

# Default java options for jmx_exporter
default[cookbook_name]['java_opts'] = nil

# Prometheus jmx_exporter config for targets
default[cookbook_name]['config'] = {}

# Configure retries for the package resources
default[cookbook_name]['package_retries'] = nil
