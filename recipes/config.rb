# frozen_string_literal: true

#
# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
targets_conf = node[cookbook_name]['config']

unless targets_conf.nil? || targets_conf.empty?
  targets_conf.each_pair do |name, conf| # rubocop:disable Metrics/BlockLength
    # Set java options for the exporter
    java_opts = node[cookbook_name]['java_opts']

    # Generate config for prometheus jmx exporter
    file "#{node[cookbook_name]['path']}/#{name}.yml" do
      content conf['options'].to_h.to_yaml
      user node[cookbook_name]['user']
      group node[cookbook_name]['group']
      mode '0600'
    end

    service "jmx_exporter_#{name}" do
      subscribes :restart, "file[#{node[cookbook_name]['path']}/#{name}.yml]",
                 :delayed
    end

    # Create systemd unit for exporter
    unit = {
      'Unit' => {
        'Description' => "jmx exporter #{name}",
        'After' => 'network.target'
      },
      'Service' => {
        'Type' => 'simple',
        'User' => node[cookbook_name]['user'],
        'Group' => node[cookbook_name]['group'],
        'Restart' => 'on-failure',
        'ExecStart' =>
          "/usr/bin/java #{java_opts} -jar \
          #{node[cookbook_name]['binary']} \
            #{conf['scrape_port']} \
            #{node[cookbook_name]['path']}/#{name}.yml"
      },
      'Install' => {
        'WantedBy' => 'multi-user.target'
      }
    }
    # Deploy systemd unit
    systemd_unit "jmx_exporter_#{name}.service" do
      enabled true
      active true
      masked false
      static false
      content unit
      triggers_reload true
      action %i[create enable start]
    end
  end
end
