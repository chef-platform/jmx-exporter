# frozen_string_literal: true

#
# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
version = node[cookbook_name]['version']

# Java is needed by jmx_exporter
java_package = node[cookbook_name]['java'][node['platform']]
package_retries = node[cookbook_name]['package_retries']

package java_package do
  retries package_retries unless package_retries.nil?
  not_if java_package.to_s.empty?
end

# Create directory, install jmx_exporter, create a symlink.
directory "jmx-exporter:#{node[cookbook_name]['path']}" do
  path node[cookbook_name]['path']
  owner node[cookbook_name]['user']
  group node[cookbook_name]['group']
  mode '0750'
  recursive true
  action :create
end

remote_file 'jmx binary with version' do
  path "#{node[cookbook_name]['path']}/jmx_exporter-#{version}.jar"
  source node[cookbook_name]['repo']
  owner node[cookbook_name]['user']
  group node[cookbook_name]['group']
end

link node[cookbook_name]['binary'] do
  to "#{node[cookbook_name]['path']}/jmx_exporter-#{version}.jar"
end
